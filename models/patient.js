var mongoose = require("mongoose");
var patientSchema = new mongoose.Schema({
    name: String,
    gender: String,
    age: Number,
    image: String,
    description: String,
    email:String,
    deviceToken: {type: String, default:0},
    avgDistance: {type: Number, default:0},
    totalDistance: {type: Number, default:0},
    recordCount: {type: Number, default:0},
    doctor:{
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
    },
    records:[
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Record"
    }
    ],
    notifications: [
      {
         type: mongoose.Schema.Types.ObjectId,
         ref: "Notification"
      }
   ]
});
    
module.exports = mongoose.model("Patient", patientSchema);